/* Header Menu */
let menuIcon = document.getElementsByClassName('js_nav-bar-button')[0];
let menuBlock = document.getElementsByClassName('navigation-menu-items')[0];
let closeIcon = document.getElementsByClassName('nav-button-hide')[0];

menuIcon.addEventListener('click',()=>{
    menuBlock.style.display = "block";
    closeIcon.style.display="block";
});

closeIcon.addEventListener('click',()=>{
    menuBlock.style.display = "none";
    closeIcon.style.display="none";
});

// Sticky Menu! BOOTSTRAP Sticky-top Doesn't work!

window.onscroll = function(){stickyScroll()};
let navbar= document.getElementsByClassName("js_sticky-nav")[0];
let sticky =navbar.offsetTop;
function stickyScroll(){
    if (window.pageYOffset>=sticky){
        navbar.classList.add('sticky')
    } else {
        navbar.classList.remove('sticky')
    }
}

/*Enable tooltips  Bootstrap*/
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

/* Menu Tabs */
$(function(){
    let $menuElement = $('.js_menu li a');
    $menuElement.click(function() {
        $menuElement.removeClass("menu-active");
        $(this).addClass("menu-active");
    });
});


// ADD TO BASKET FUNCTIONALITY:
let basketStatus = $('.header-basket-access');
let basketItemCounterDisplay =$('.js_basket-counter');
let basketItemCounter=0;
//Initial Basket Setup
basketItemCounterDisplay.text(`${basketItemCounter}`);
basketStatus.attr('data-target', '#basketEmptyModal');

// "Add to Basket" Button functionality:
$('.product-furniture-gallery__item-img-add').click(function() {
  let itemData=JSON.parse((this).getAttribute('data-info'));
        basketItemCounter+=1;
        basketStatus.attr('data-target', '#basketFullModal');
        basketItemCounterDisplay.text(`${basketItemCounter}`);
    $('.js_basket_modal_list_items').prepend(`
     <div class="row product-item-wrapper product-added">
        <div class="col-12 col-lg-6 product-added-item">
              <div  class="product-added-item-image">
                <img class="product-added-item-pic" alt="product image" src="${itemData.img}" >
            </div>
            <div class="product-added-item-description">
                <p class="product-added-item-title">${itemData.name}</p>
                <div class="product-added-item-title-prices">
                <p class="product-added-item-old-price"><del>$350.00</del></p>
                <p class="product-added-item-new-price">${itemData.price}</p>
                </div>
        </div>
        </div>
        <div class="col-12 col-lg-6 product-added-quantity">
            <div class="product-added-quantity-section">
            <span class="product-added-quantity-text">Quantity:</span>
                <div class="product-added-quantity-wrapper">
            <input type="number" class="rounded-0 border border-dark product-added-quantity-input" aria-label="product-added-quantity-input" placeholder="2">
            <button class="btn product-added-quantity-btn rounded-0">-</button>
            <button class="btn product-added-quantity-btn rounded-0">+</button>
                </div>
            </div>
            <div class="product-added-sum">
                <span class="product-added-sum-text">Sum</span>
                <div class="product-added-sum-number">${itemData.price}</div>
            </div>

        </div>
    </div>`
           )
    });


// Quick View Modal!!
// THE SAME FOR ITEMS IN MAIN LIST (.product-furniture-gallery__item-img-view)
$('.js_product_modal_item-open-modal').click(function(){
    let itemFurnitureData=JSON.parse((this).getAttribute('data-info'));
    $('.js_product_modal_item').prepend(`
<div class="js_product_modal_item-added js_product_modal_item-added row product-details-modal-wrapper">
     <div class="col-12 col-lg-6 product-details-modal-wrapper-image">
            <img class="product-details-modal-image" src="${itemFurnitureData.img}" alt="product-image">
        </div>
        <div class="col-12 col-lg-6">
            <h4 class="modal-title product-details-modal-title">${itemFurnitureData.name}</h4>

                <div class="product-details-modal-review-section">
                    <ul class="product-furniture-gallery__item-rating d-flex justify-content-center">
                        <li class="product-furniture-gallery__item-rating-star"><i class="fas fa-star"></i></li>
                        <li class="product-furniture-gallery__item-rating-star"><i class="fas fa-star"></i></li>
                        <li class="product-furniture-gallery__item-rating-star"><i class="far fa-star"></i></li>
                        <li class="product-furniture-gallery__item-rating-star"><i class="far fa-star"></i></li>
                        <li class="product-furniture-gallery__item-rating-star"><i class="far fa-star"></i></li>
                    </ul>
                    <span class="product-details-modal-review-description text-nowrap">(0 reviews)</span>

                </div>

            <div class="product-details-modal-price">
                <span class="product-details-modal-price-old"><del>${itemFurnitureData.oldPrice}</del></span>
                <span class="product-details-modal-price-new">${itemFurnitureData.price}</span>
            </div>
            <div class="product-details-modal-add">
                <a href="#" class="product-details-modal-add-button"><i class="fas fa-shopping-basket pr-2"></i> Add to cart</a>
                <a href="#" class="product-details-modal-add-heart"><i class="far fa-heart product-details-modal-add-heart-icon"></i></a>
            </div>

            <div class="product-details-modal-overview-section">
                <h3 class="product-details-modal-overview-section-title">Quick Overview</h3>
                <p class="product-details-modal-overview-section-description">Nam tristique porta ligula, vel viverra sem eleifend nec. Nulla sed purus augue, eu euis mod
                    tellus. Nam mattis eros tis sagittis. Vestibulum suscipit cursus biben.</p>
            </div>
            <div class="product-details-modal-overview-category">
                <span class="product-details-modal-overview-category-availability"><span class="font-weight-bold">Availability:</span> In Stock</span>
                <span class="product-details-modal-overview-category-category"><span class="font-weight-bold">Category:</span> Furniture</span>
            </div>
        </div> 
    </div>`
    )
});
//delete DIV on close Product Modal
$('.js_modal_close').click(function () {
    $('.js_product_modal_item').empty();
    }
);


// MULTIRANGE
(function() {
    "use strict";

    var supportsMultiple = self.HTMLInputElement && "valueLow" in HTMLInputElement.prototype;

    var descriptor = Object.getOwnPropertyDescriptor(HTMLInputElement.prototype, "value");

    var multirange = function(input) {
        if (supportsMultiple || input.classList.contains("multirange")) {
            return;
        }

        var value = input.getAttribute("value");
        var values = value === null ? [] : value.split(",");
        var min = +(input.min || 0);
        var max = +(input.max || 100);
        var ghost = input.cloneNode();
        var dragMiddle = input.getAttribute("data-drag-middle") !== null;
        var middle = input.cloneNode();

        input.classList.add("multirange", "original");
        ghost.classList.add("multirange", "ghost");

        input.value = values[0] || min + (max - min) / 2;
        ghost.value = values[1] || min + (max - min) / 2;

        input.parentNode.insertBefore(ghost, input.nextSibling);

        Object.defineProperty(input, "originalValue", descriptor.get ? descriptor : {
            // Fuck you Safari >:(
            get: function() { return this.value; },
            set: function(v) { this.value = v; }
        });

        Object.defineProperties(input, {
            valueLow: {
                get: function() { return Math.min(this.originalValue, ghost.value); },
                set: function(v) { this.originalValue = v; },
                enumerable: true
            },
            valueHigh: {
                get: function() { return Math.max(this.originalValue, ghost.value); },
                set: function(v) { ghost.value = v; },
                enumerable: true
            }
        });

        if (descriptor.get) {
            // Again, fuck you Safari
            Object.defineProperty(input, "value", {
                get: function() { return this.valueLow + "," + this.valueHigh; },
                set: function(v) {
                    var values = v.split(",");
                    this.valueLow = values[0];
                    this.valueHigh = values[1];
                    update();
                },
                enumerable: true
            });
        }

        if (typeof input.oninput === "function") {
            ghost.oninput = input.oninput.bind(input);
        }

        function update(mode) {
            ghost.style.setProperty("--low", 100 * ((input.valueLow - min) / (max - min)) + 1 + "%");
            ghost.style.setProperty("--high", 100 * ((input.valueHigh - min) / (max - min)) - 1 + "%");

            if (dragMiddle && mode !== 1) {
                let w = input.valueHigh - input.valueLow;
                if (w>1) w-=0.5;
                middle.style.setProperty("--size", (100 * w / (max - min)) + "%");
                middle.value = min + (input.valueHigh + input.valueLow - 2*min - w)*(max - min)/(2*(max - min - w));
            }
        }

        ghost.addEventListener("mousedown", function passClick(evt) {
            // Find the horizontal position that was clicked
            let clickValue = min + (max - min)*evt.offsetX / this.offsetWidth;
            let middleValue = (input.valueHigh + input.valueLow)/2;
            if ( (input.valueLow == ghost.value) == (clickValue > middleValue) ) {
                // Click is closer to input element and we swap thumbs
                input.value = ghost.value;
            }
        });
        input.addEventListener("input", update);
        ghost.addEventListener("input", update);

        if (dragMiddle) {
            middle.classList.add("multirange", "middle");
            input.parentNode.insertBefore(middle, input.nextSibling);
            middle.addEventListener("input", function () {
                let w = input.valueHigh - input.valueLow;
                let m = min + w/2 + (middle.value - min)*(max - min - w)/(max-min);
                input.valueLow = m - w/2;
                input.valueHigh = input.valueLow+w;
                update(1);
            });
        }

        update();
    };

    multirange.init = function() {
        [].slice.call(document.querySelectorAll("input[type=range][multiple]:not(.multirange)")).forEach(multirange);
    };

    if (typeof module === "undefined") {
        self.multirange = multirange;
        if (document.readyState == "loading") {
            document.addEventListener("DOMContentLoaded", multirange.init);
        }
        else {
            multirange.init();
        }
    } else {
        module.exports = multirange;
    }

})();



// LOAD MORE
const loadBtn = document.querySelector('.product-gallery__load-more');
const loadElements = document.querySelectorAll('.product-furniture-gallery__item-more');
const loadIcon = document.querySelector('.fa-sync-alt');

loadBtn.addEventListener('click', function () {
    loadIcon.classList.add('product-gallery__load-more-icon-move');
    setTimeout(displayNone, 2000, loadElements);
});

function displayNone(elements) {
    [...elements].forEach((item) => {
        item.classList.remove('d-none')});
        loadBtn.classList.add('d-none');
}





