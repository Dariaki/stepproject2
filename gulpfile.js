'use strict';

const gulp = require('gulp'),
      sass = require('gulp-sass');
const browserSync = require('browser-sync').create();


function copyHtml() {
    return gulp.src('src/index.html')
        .pipe(gulp.dest('build/'))
}


function copyJS() {
    return gulp.src('src/assets/js/main.js')
        .pipe(gulp.dest('build/assets/js/'))
        .pipe(browserSync.stream())
}

function sassToCss() {
    return gulp.src('src/assets/sass/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('build/assets/css/'))
        .pipe(browserSync.stream())
}


function watch() {
    gulp.watch('src/index.html', copyHtml).on('change', browserSync.reload);
    gulp.watch('src/assets/sass/*.scss', sassToCss);
    gulp.watch('src/assets/js/*.js', copyJS)
}

function serve() {
    browserSync.init({
        server: './build'
    });
    watch();
}

exports.build = gulp.series(copyHtml, sassToCss, copyJS);
exports.watch = watch;
exports.sass = sassToCss;
exports.serve = serve;



